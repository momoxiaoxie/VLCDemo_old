package com.daimc.vlcdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import org.videolan.libvlc.PlayerView;

public class MainActivity extends AppCompatActivity implements PlayerView.OnChangeListener {
    private  static  final  String VIDEO_URL="rtsp://troy:troy1227@218.6.203.178:554/cam/realmonitor?channel=1&subtype=0";
    private PlayerView mPlayerView;
    private UniversalMediaController mediaController;
    private View mVideoLayout;
    private boolean isFullscreen;
    private int cachedHeight;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mVideoLayout = findViewById(R.id.video_layout);
        mPlayerView = (PlayerView) findViewById(R.id.videoView);
        mediaController = (UniversalMediaController) findViewById(R.id.media_controller);
        mPlayerView.setMediaController(mediaController);
        mPlayerView.setOnChangeListener(this);
        mediaController.setTitle("video");
        mPlayerView.initPlayer(VIDEO_URL);
        mPlayerView.startVlc();
        setVideoAreaSize();
    }
    /**
     * 置视频区域大小
     */
    private void setVideoAreaSize() {
        mVideoLayout.post(new Runnable() {
            @Override
            public void run() {
                int width = mVideoLayout.getWidth();
                // cachedHeight = (int) (width * 405f / 720f);
                // cachedHeight = (int) (width * 3f / 4f);
                cachedHeight = (int) (width * 9f / 16f);
                ViewGroup.LayoutParams videoLayoutParams = mVideoLayout
                        .getLayoutParams();
                videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                videoLayoutParams.height = cachedHeight;
                mVideoLayout.setLayoutParams(videoLayoutParams);
            }
        });
    }
    @Override
    public void onBufferChanged(float buffer) {

    }

    @Override
    public void onLoadComplet() {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onEnd() {

    }

    @Override
    public void onScaleChange(boolean isFull) {
        this.isFullscreen = isFull;
        if (isFullscreen) {
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
            ViewGroup.LayoutParams layoutParams = mVideoLayout
                    .getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mVideoLayout.setLayoutParams(layoutParams);
        } else {
            this.requestWindowFeature(Window.FEATURE_ACTION_BAR);//去掉标题栏
            ViewGroup.LayoutParams layoutParams = mVideoLayout
                    .getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.height = this.cachedHeight;
            mVideoLayout.setLayoutParams(layoutParams);
        }
    }
}
