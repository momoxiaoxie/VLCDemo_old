package com.daimc.vlcdemo;

import android.app.Application;

/**
 * @author daimingcheng
 * @time 2016/8/25 10:51
 */
public class App extends Application{
    public static App INSTANCE;
    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
    }
    public static App getInstance() {
        return INSTANCE;
    }

}
